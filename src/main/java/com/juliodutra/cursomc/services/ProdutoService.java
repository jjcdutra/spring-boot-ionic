package com.juliodutra.cursomc.services;

import com.juliodutra.cursomc.domains.Categoria;
import com.juliodutra.cursomc.domains.Produto;
import com.juliodutra.cursomc.repositories.CategoriaRepository;
import com.juliodutra.cursomc.repositories.ProdutoRepository;
import com.juliodutra.cursomc.services.exception.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired private ProdutoRepository repo;
    @Autowired private CategoriaRepository categoriaRepository;

    public Produto find(Integer id) {
        Optional<Produto> obj = repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado, id: " + id + ", tipo: " + Produto.class.getName()));
    }

    public Page<Produto> search(String nome, List<Integer> ids, Integer page, Integer linesPerPage, String orderBy, String direction) {
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        List<Categoria> categorias = categoriaRepository.findAllById(ids);
        return repo.findDistinctByNomeContainingAndAndCategoriasIn(nome, categorias, pageRequest);
    }
}
