package com.juliodutra.cursomc.config;

import com.juliodutra.cursomc.services.DBService;
import com.juliodutra.cursomc.services.EmailService;
import com.juliodutra.cursomc.services.MockMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;

@Configuration
@Profile("test")
public class TestConfig {

    @Autowired private DBService dbService;

    @Bean
    public boolean inititiationDataBase() throws ParseException {

        dbService.instantiateTestDataBase();

        return true;
    }

    @Bean
    public EmailService emailService() {
        return new MockMailService();
    }
}
